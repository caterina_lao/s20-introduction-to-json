/*Arrow function*/
// function sampleFunction(){
// 	alert(`Hello, I am Filipino.`)
// }
// sampleFunction();

// const sampleFunction1 = () => {
// 	alert(`Hello, I am an arrow function.`);
// };

// sampleFunction1();

// implicit return with arrow function
function prodNum(num1, num2) {
	return num1 * num2
};

let prod = prodNum(25, 25);
console.log(prod);

// arrow function
const prodNum1 = (num1, num2) => num1 * num2;
let product = prodNum1(25, 10);
console.log(product);

const multiplyNum = (num1, num2) => {
	return num1*num2
};

let prod2 = multiplyNum(5, 10);
console.log(prod2);

function ans(a){
	if(a < 10) {
		return 'true';
	} else {
		return 'false';
	}
};

let answer = ans(5);
console.log(answer);

// Ternary Operation
// Question mark (?) - true ; colon (:) - false
const ans1 = (a1) => (a1 < 10) ? 'true' : 'false';
let answer1 = ans1(5);
console.log(answer1);

let mark = prompt('Enter your grade:');

let grade = (mark >= 75) ? 'Pass' : 'Fail'
console.log(`You ${grade} the exam.`);


/*
	Convert the ff function into an arrow function and display the result in console.

	let numbers = [1 ,2, 3, 4, 5];
	let allValid = numbers.every(function(number){
	return (number < 3)
	});

	let filterValid = numbers.filter(function(number){
	return (number < 3)
	});

	let numberMap = numbers.map(function(number){
	return number * number
	});

*/
let numbers = [1 ,2, 3, 4, 5];

const allValid = numbers.every(numbers => numbers < 3);
console.log(allValid);

const filterValid = numbers.filter(numbers => numbers < 3)
console.log(filterValid);;
	
const numberMap = numbers.map(numbers => numbers < 3 )
console.log(numberMap);
	
//to check if the number is positive, negative or zero
let num = 3;
let result = (num >= 0) ? (num ==0 ? 'zero' : 'positive') : 'negative';
console.log(`The number is ${result}.`);

//JSON - JavaScript Object Notation
	// JSON is a string but formatted as JS Object
	//JSON is popularly used to pass data from one application to another
	//JSON is not noly in JS bit also in other programming languages to pass data
	//this is why it is specified as JavaScript Object Notation
	// file extension - .json
	//There is a way to turn JSON as JS Objects and there is way to turn JS Objects to JSON


//***JS Objects are not the same as JSON
	//JSON is a string
	//JS Objects is an object
	//JSON keys are surrounded with double quotes.


//**Syntax:

/*
	{
	"key1" : "value1",
	"key2" : "value2",
	}

-string
-number
-object
-array
-boolean
-null
*/

let person = {
	"name" : "Juan",
	"weight" : 175,
	"age" : 20,
	"eyeColor" : "brown",
	"cars" : ["Toyota", "Honda"],
	"favoriteBooks" : {
		"title" : "When the fire Nation Attack",
		"author" : "Nickelodeon",
		"release" : "2021"
	}
}
console.log(typeof(person)); //object

/*
		Mini-activity:
	
	Create an array of JSON format datas with atleast 2 items.
	Name the array as assets
	Each JSON format data should have the following key-value pairs:

		id - <>
		name - <value>
		description - <value>
		isAvailable - <value>
		dateAdded - <value>
*/

// let assets1 = [{
// 	"id" : "2021-A",
// 	"name" : "Desktop",
// 	"description" : "electronic device",
// 	"isAvailable" : true,
// 	"dateAdded" : "Oct. 13, 2021"
// }];

// console.log(assets1);

// let assets2 = [{
// 	"id" : "2021-B",
// 	"name" : "Computer table",
// 	"description" : "table for desktop",
// 	"isAvailable" : false,
// 	"dateAdded" : "Oct. 13, 2021"
// }];

let assets1 = [
{
	"id" : "2021-A",
	"name" : "Desktop",
	"description" : "electronic device",
	"isAvailable" : true,
	"dateAdded" : "Oct. 13, 2021"
},
{
	"id" : "2021-B",
	"name" : "Computer table",
	"description" : "table for desktop",
	"isAvailable" : false,
	"dateAdded" : "Oct. 13, 2021"
}];
console.log(assets1);

//XML format - extensible markup language

/*
	<note>
		<to>Juan</to>
		<from>Maria Clara</from>
		<heading>Reminder</heading>

		<body>Don't forget me this weekend!</body>
	</note>
*/
//JSON - key value pairs - string

// {
// 	"to" : "Juan",
// 	"from" : "Maria Clara",
// 	"heading" : "Reminder",
// 	"body" : "Don't forget me this weekend!"
// };

// let assets = [
// 	{
// 		"id" : "item-1",
// 		"name" : "Construction Crane",
// 		"description" : "Doesn't actually fly",
// 		"isAvailable" : true,
// 		"dateAdded" : "Oct. 13, 2007"
// 	},
// 	{
// 		"id" : "item-2",
// 		"name" : "Backhoe",
// 		"description" : "It has claw",
// 		"isAvailable" : true,
// 		"dateAdded" : "Jan. 13, 2009"
// 	},
// ];

//Stringify - method to convert JavaScript object to JSON and vice versa
//Converting JS Objects into JSON
	//this is commonly used when trying to pass data from one application to another via the use HTTP Request.
	//HTTP requests are requests from resource between server and a client(browser)
	//JSON format is also used in database

const cat = {
	"name" : "Mashiro",
	"age" : 3,
	"weight" : 20
};

console.log(cat);

//using stringify

const catJSON = JSON.stringify(cat);
console.log(catJSON);

//using parse - reverse of using stringify
const json = '{"name":"Mashiro","age":3,	"weight":20}';
const cat1 = JSON.parse(json);
console.log(cat1);

let batchesArr = [
	{
		batchName : "Batch 131",
	},
	{
		batchName : "Batch 132",
	},
];
console.log(JSON.stringify(batchesArr));

let batchesJSON = `[
	{
		"batchName":"Batch 131"
	},
	{
		"batchName":"Batch 132"
	}
	]`;
	console.log(JSON.parse(batchesJSON));

/*
	Create an arrow function called postCourse which allows us to a new object into the array. It should receive data: id, name, description, price, isActive.
		-push()

	Add the new course into the array and show the following alert:
	"You have created <nameOfCourse>. The price is <priceOfCourse>."
		-literals

	Create an arrow function which allows us to find a particular course providing the course id and return the details of the found course.
		-use find

	Create an arrow function called deleteCourse which can delete the last course object in the array.
		-pop()

*/
const courseArr = []

const postCourse = (id, name, description, price) => {
	courseArr.push(
	{		
		id : id,
		name : name,
		description : description,
		price : price
	})
	alert(`You have created ${name}. The price is ${price}.`)
};

postCourse('2021-BS001', 'BS in Accounting Technology', 'Basic Accounting and Finance Skills Necessary', 7500);

postCourse('2021-BS002', 'BS in Accountancy', 'Practice of recording, classifying, and reporting on business transactions for a business', 85200);

postCourse('2021-BS003', 'BS in Marketing Management', 'Specialize in advertising and marketing', 64300);



const foundCourse = courseArr.find(({name}) => name === 'BS in Accounting Technology');
	console.log(foundCourse);


const deleteCourse = courseArr.pop();
	console.log(courseArr);
	console.log(deleteCourse);
