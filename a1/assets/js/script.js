/*
	Create an arrow function called postCourse which allows us to a new object into the array. It should receive data: id, name, description, price, isActive.
		-push()

	Add the new course into the array and show the following alert:
	"You have created <nameOfCourse>. The price is <priceOfCourse>."
		-literals

	Create an arrow function which allows us to find a particular course providing the course id and return the details of the found course.
		-use find

	Create an arrow function called deleteCourse which can delete the last course object in the array.
		-pop()

*/
const courseArr = []

const postCourse = (id, name, description, price) => {
	courseArr.push(
	{		
		id : id,
		name : name,
		description : description,
		price : price
	})
	alert(`You have created ${name}. The price is ${price}.`)
};

postCourse('2021-BS001', 'BS in Accounting Technology', 'Basic Accounting and Finance Skills Necessary', 7500);

postCourse('2021-BS002', 'BS in Accountancy', 'Practice of recording, classifying, and reporting on business transactions for a business', 85200);

postCourse('2021-BS003', 'BS in Marketing Management', 'Specialize in advertising and marketing', 64300);



const foundCourse = courseArr.find(({name}) => name === 'BS in Accounting Technology');
	console.log(foundCourse);


const deleteCourse = courseArr.pop();
	console.log(courseArr);
	console.log(deleteCourse);